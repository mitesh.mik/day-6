﻿using CollectionList.Exception;
using CollectionList.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionList.Repository

{

    internal class ProductRepository
    {
        List<Product> products;
        public ProductRepository()
        {
            products = new List<Product>()
            {
                new Product() { Id = 1, Name = "hp", Price = 30000 },
                new Product() { Id = 2, Name = "redmi", Price = 4000 }

            };

           

        }
       public  List<Product> getAllProducts()
        {
            return products;
        }
        public string AddProduct(Product product)
        {
            //products.Add(product);
            var productExists = getByCategory(product.Name);
            if (productExists == null)
            {
                products.Add(product);
                return $"product added.";
            }
            else
            {
                throw new ProductException("Product already exists");
            }
        }

        public Product getByCategory(string name)
        {
            return products.Find(p =>p.Name==name)  ;
        }

        public bool deleteProductByName(string name)
        {
            var pro=getByCategory(name);
            return  pro != null ? products.Remove(pro) : false;
        }
    }
}
