﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionList.Exception
{
    internal class ProductException:ApplicationException
    {
        public ProductException()
        {

        }
        public ProductException(string Message):base(Message)
        {

        }
    }
}
