﻿// See https://aka.ms/new-console-template for more information
using SortedListDemo;
using System.Collections;

Console.WriteLine("sortedist");
SortedList sortedlist = new SortedList();
sortedlist.Add(101, new Student() { Id = 101, Name = "mik", Age = 20 });
sortedlist.Add(90, new Student() { Id = 90, Name = "nik", Age = 21 });
foreach (DictionaryEntry student in sortedlist)
{
    Console.WriteLine(student.Value);
}
//var descendingComparer = Comparer<int>.Create((x, y) => y.CompareTo(x));
//SortedList desc = (descendingComparer)new SortedList();
//desc.Add(101, new Student() { Id = 101, Name = "mik", Age = 20 });
//desc.Add(90, new Student() { Id = 90, Name = "nik", Age = 21 });
//foreach (DictionaryEntry student in desc)
//{
//    Console.WriteLine(student.Value);
//}


