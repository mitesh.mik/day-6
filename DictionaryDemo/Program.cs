﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("demo dictionary");

Dictionary<int, string> courses = new Dictionary<int, string>();
//add items 
courses.Add(101, "c#");
courses.Add(102, "Angular");
courses.Add(103, "Asp.Net");
foreach (KeyValuePair<int,string> course in courses)
{
    Console.WriteLine($"key:{course.Key} \t value:{course.Value}");
}
Console.WriteLine($"contain key:{courses.ContainsKey(101)}");
courses.Remove(103);
foreach (KeyValuePair<int,string> course in courses)
{
    Console.WriteLine($"key:{course.Key} \t value:{course.Value}");
}
